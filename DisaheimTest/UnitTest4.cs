using Disaheim;

namespace DisaheimTest;

[TestClass]
public class UnitTest4
{
    private Amulet a1, a2, a3;
    private Book b1, b2, b3;
    private Course c1, c2;

    private ValuableRepository Course, books, amulets;

    [TestInitialize]
    public void Init()
    {
        // Arrange
        b1 = new Book("1");
        b2 = new Book("2", "Falling in Love with Yourself");
        b3 = new Book("3", "Spirits in the Night", 123.55);

        a1 = new Amulet("11");
        a2 = new Amulet("12", Level.High);
        a3 = new Amulet("13", Level.Low, "Capricorn");

        c1 = new Course("Eufori med røg");
        c2 = new Course("Nuru Massage using Chia Oil", 157);

        Course = new ValuableRepository();
        books = new ValuableRepository();
        amulets = new ValuableRepository();

        // Act
        books.AddValuable(b1);
        books.AddValuable(b2);
        books.AddValuable(b3);

        amulets.AddValuable(a1);
        amulets.AddValuable(a2);
        amulets.AddValuable(a3);

        Course.AddValuable(c1);
        Course.AddValuable(c2);
    }

    [TestMethod]
    public void TestGetBook()
    {
        // Assert
        Assert.AreEqual(b2, books.GetValuable("2"));
    }

    [TestMethod]
    public void TestGetAmulet()
    {
        // Assert
        Assert.AreEqual(a3, amulets.GetValuable("13"));
    }

    [TestMethod]
    public void TestGetCourse()
    {
        // Assert
        Assert.AreEqual(c1, Course.GetValuable("Eufori med røg"));
    }

    [TestMethod]
    public void TestGetTotalValueForBook()
    {
        // Assert
        Assert.AreEqual(123.55, books.GetTotalValue());
    }

    [TestMethod]
    public void TestGetTotalValueForAmulet()
    {
        // Assert
        Assert.AreEqual(60.0, amulets.GetTotalValue());
    }

    [TestMethod]
    public void TestGetTotalValueForCourse()
    {
        // Assert
        Assert.AreEqual(2625.0, Course.GetTotalValue());
    }
}