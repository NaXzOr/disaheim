using Disaheim;

namespace DisaheimTest;

[TestClass]
public class UnitTest3
{
    private Amulet a1, a2, a3;
    private Book b1, b2, b3;
    private Course c1, c2, c3;

    private Controller controller;

    [TestInitialize]
    public void Init()
    {
        // Arrange

        b1 = new Book("1");
        b2 = new Book("2", "Falling in Love with Yourself");
        b3 = new Book("3", "Spirits in the Night", 123.55);

        a1 = new Amulet("11");
        a2 = new Amulet("12", Level.High);
        a3 = new Amulet("13", Level.Low, "Capricorn");

        c1 = new Course("Spådomskunst for nybegyndere");
        c2 = new Course("Magi – når videnskaben stopper", 157);
        c3 = new Course("Et indblik i Helleristning", 180);

        controller = new Controller();

        controller.AddToList(b1);
        controller.AddToList(b2);
        controller.AddToList(b3);

        controller.AddToList(a1);
        controller.AddToList(a2);
        controller.AddToList(a3);

        controller.AddToList(c1);
        controller.AddToList(c2);
        controller.AddToList(c3);
    }

    [TestMethod]
    public void TestBookList()
    {
        // Assert
        Assert.AreEqual(b3, controller.ValuableRepo[2]);
    }

    [TestMethod]
    public void TestAmuletList()
    {
        // Assert
        Assert.AreEqual(a1, controller.ValuableRepo[3]);
    }

    [TestMethod]
    public void TestCourseList()
    {
        // Assert
        Assert.AreEqual(c1, controller.ValuableRepo[6]);
        Assert.AreEqual(c2, controller.ValuableRepo[7]);
        Assert.AreEqual(c3, controller.ValuableRepo[8]);
    }
}