namespace Disaheim;

public class Course : IValuable
{
    public string Name;

    public Course(string name)
    {
        Name = name;
    }

    public Course(string name, int durationInMinutes) : this(name)
    {
        DurationInMinutes = durationInMinutes;
    }

    public static double CourseHourValue { get; set; } = 875.00;
    public int DurationInMinutes { get; set; }

    public string Id => Name;

    public double GetValue()
    {
        double price;
        var one = DurationInMinutes;
        var two = 60;

        double Price()
        {
            var result = one / two;
            if (one % two != 0) result++;
            price = result * CourseHourValue;
            return price;
        }

        return Price();
    }

  

    public override string ToString()
    {
        return $"Name: {Name}, Duration in Minutes: {DurationInMinutes}, Value: {GetValue()}";
    }
}