namespace Disaheim;

public class Book : Merchandise
{
    public Book(string inItemId)
    {
        ItemId = inItemId;
    }

    public Book(string inItemId, string inTitle) : this(inItemId)
    {
        Title = inTitle;
    }

    public Book(string inItemId, string inTitle, double inValue) : this(inItemId, inTitle)
    {
        Value = inValue;
    }

    public string Title { get; set; }
    public double Value { get; set; }

    public override double GetValue()
    {
        return Value;
    }

    public override string ToString()
    {
        return $"ItemId: {ItemId}, Title: {Title}, Price: {Value}";
    }
}