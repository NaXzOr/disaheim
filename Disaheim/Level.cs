namespace Disaheim;

public enum Level
{
    Medium,
    Low,
    High
}