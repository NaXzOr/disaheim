namespace Disaheim;

public class ValuableRepository : IPersistable
{
    private readonly List<IValuable> _valuables;


    public ValuableRepository()
    {
        _valuables = new List<IValuable>();
    }

    public void AddValuable(IValuable valuable)
    {
        _valuables.Add(valuable);
    }

    public IValuable GetValuable(string id)
    {
        return _valuables.Single(x => string.Equals(x.Id, id, StringComparison.InvariantCultureIgnoreCase));
    }

    public double GetTotalValue()
    {
        double totalValue = 0;
        foreach (var valuable in _valuables)
            totalValue += valuable.GetValue();

        return totalValue;
    }

    public int Count()
    {
        return _valuables.Count;
    }

    public void Save(string fileName = "ValuableRepository.txt")
    {
        using var writer = new StreamWriter(fileName);
        foreach (var valuable in _valuables)
        {
            var line = valuable switch
            {
                Book book => $"{book.GetType().Name};{book.ItemId};{book.Title};{book.Value}",
                Amulet amulet => $"{amulet.GetType().Name};{amulet.ItemId};{amulet.Design};{amulet.Quality}",
                Course course => $"{course.GetType().Name};{course.Name};{course.DurationInMinutes};{course.GetValue()}",
                _ => throw new Exception("YOU FUCKED UP")
            };
            writer.WriteLine(line);
        }
    }

    public void Load(string fileName = "ValuableRepository.txt")
    {
        using var reader = new StreamReader(fileName);
        while (reader.ReadLine() is { } hasValue)
        {
            var valuableData = hasValue.Split(";");
            var valuable = GetValuable(valuableData);
            _valuables.Add(valuable);
        }
    }

    private static IValuable GetValuable(string[] valuableData)
    {
        switch (valuableData[0])
        {
            case nameof(Book):
            {
                var book = CreateBook(valuableData);

                return book;
            }
            case nameof(Amulet):
            {
                var amulet = CreateAmulet(valuableData);

                return amulet;
            }
            case nameof(Course):
            {
                var course = CreateCourse(valuableData);

                return course;
            }
        }

        throw new ArgumentException($"fail in input data {valuableData[0]}");
    }

    private static Course CreateCourse(string[] valuableData)
    {
        var course = new Course(valuableData[0]);
        if (valuableData.Length > 1)
        {
            course.Name = valuableData[1];
        }

        if (valuableData.Length > 2)
        {
            course.DurationInMinutes = int.Parse(valuableData[2]);
        }

        return course;
    }

    private static Amulet CreateAmulet(string[] valuableData)
    {
        var amulet = new Amulet(valuableData[1]);
        if (valuableData.Length > 2)
        {
            amulet.Design = valuableData[2];
        }
        if (valuableData.Length > 3)
        {
            amulet.Quality = Enum.Parse<Level>(valuableData[3]);
        }

        

        return amulet;
    }

    private static Book CreateBook(string[] valuableData)
    {
        var book = new Book(valuableData[1]);
        if (valuableData.Length > 2)
        {
            book.Title = valuableData[2];
        }

        if (valuableData.Length > 3)
        {
            book.Value = double.Parse(valuableData[3]);
        }

        return book;
    }
}