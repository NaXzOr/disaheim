namespace Disaheim;

public abstract class Merchandise : IValuable
{
    public string ItemId { get; set; }
    public string Id => ItemId;
    public abstract double GetValue();

    public override string ToString()
    {
        return $"ItemId: {ItemId}";
    }
}