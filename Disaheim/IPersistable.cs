using System.IO.Enumeration;
using System.Xml.Linq;

namespace Disaheim;

public interface IPersistable
{
    
    public string Save()
    {
        var fileName = Save();
        return Save();
    }

    public string Load()
    {
        var fileName = Load();
        return Load();
    }
    
}