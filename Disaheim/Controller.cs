namespace Disaheim;

public class Controller : ValuableRepository
{
    public Controller()
    {
        ValuableRepo = new List<IValuable>();
    }

    public List<IValuable> ValuableRepo { get; set; }

    public void AddToList(IValuable valuable)
    {
        ValuableRepo.Add(valuable);
    }
}