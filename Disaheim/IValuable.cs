namespace Disaheim;


public interface IValuable
{
    string Id { get; }
    double GetValue();
    

}